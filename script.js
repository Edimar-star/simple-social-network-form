document.getElementById('registrationForm').addEventListener('submit', function(event) {
    event.preventDefault();
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const birthdate = document.getElementById('birthdate').value;
    const gender = document.querySelector('input[name="gender"]:checked')?.value;

    if (!firstName || !lastName || !email || !password || !birthdate || !gender) {
        alert('Por favor, complete todos los campos correctamente.');
    } else {
        alert('Registro completado con éxito!');
    }
});
